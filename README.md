
noblogsmv
=========

*noblogsmv* is a script that drives the transition between two
different backend maps for the Noblogs databases. The process is meant
to be driven by a single script (the *driver*), running on a single
host. This script will save its state locally, so that it is easily
restartable in case it is interrupted.


## Installation

To install from source in a Python virtualenv:

    $ virtualenv venv
    $ ./venv/bin/pip install git+https://git.autistici.org/ai/noblogsmv.git#egg=noblogsmv

This will install all the required dependencies and it will put the
driver script in `venv/bin/`.

## Usage

When performing a backend migration, you need first to have the
two blog->backend maps ("old" and "new") in JSON format. You can
get them by running the 'noblogs' admin command with suitable
arguments.

Then, create a new, empty directory on the driver host:

    $ mkdir /var/tmp/migration-01
    $ cd /var/tmp/migration-01

And run the driver script:

    $ sudo screen noblogsmv-driver \
        --old-map=old.json \
        --new-map=new.json

The web console will be available on `http://localhost:3030/`.
