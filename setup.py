#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="noblogsmv",
    version="0.2.1",
    description="Parallel FSM for Noblogs operations",
    author="ale",
    author_email="ale@incal.net",
    url="https://git.autistici.org/ai/noblogsmv",
    install_requires=['Flask', 'leveldb', 'pygooglechart'],
    setup_requires=[],
    zip_safe=True,
    packages=find_packages(),
    package_data={"noblogsmv": ["templates/*", "static/img/*", "static/css/*", "static/js/*"]},
    entry_points={
        "console_scripts": [
            "noblogs-map-diff = noblogsmv.noblogs_map_diff:main",
            "noblogsmv = noblogsmv.main:main",
            ],
        },
    )
