# Computes the difference between two Noblogs blog->backend maps,
# usually an OLD and a NEW one, printing on standard output the list
# of blogs where OLD != NEW in a format that can be digested by the
# noblogsmv driver.

import json
import logging
import optparse
import re
import sys

from noblogsmv import state

log = logging.getLogger('maps-diff')

MYSQL_URI_RE = re.compile(
    r'^(\d+)\s+\S+\s+mysql://([^:]+):([^@]+)@([^:]+):(\d+)/(\S+)')


def generate_diff_from_maps(old_map, old_backend, new_map, new_backend):
    """Generate the diff map from old and new blog/backend maps."""

    # Maps are in the form backend_id -> [blog_id, ...]
    # Parse them appropriately.
    def _to_blogs(m):
        out = {}
        for k, values in m.iteritems():
            for v in values:
                out[v] = k
        return out
    old_blogs = _to_blogs(old_map)
    new_blogs = _to_blogs(new_map)

    def _backend_params(bmap, id, pfx):
        return {pfx + '_user': bmap[id]['user'],
                pfx + '_pass': bmap[id]['pass'],
                pfx + '_host': bmap[id]['host'],
                pfx + '_port': bmap[id]['port'],
                pfx + '_db': bmap[id]['db']}

    # We need to process blogs that meet the following criteria:
    # - they appear in the 'new' map
    # - they appear in the 'old' map
    # - the backend values are different
    diff = {}
    for blog_id in new_blogs.iterkeys():
        if blog_id not in old_blogs:
            log.error('Blog ID #%d disappeared!', blog_id)
            continue
        new_bid = new_blogs[blog_id]
        old_bid = old_blogs[blog_id]
        if new_bid == old_bid:
            continue
        diff[blog_id] = {
            'new_server_id': new_bid,
            'old_server_id': old_bid,
            }
        diff[blog_id].update(_backend_params(new_backend, new_bid, 'new'))
        diff[blog_id].update(_backend_params(old_backend, old_bid, 'old'))

    log.info('%d total blogs, %d need to be moved (%.2g%%)',
             len(new_blogs), len(diff), float(len(diff)) / len(new_blogs))

    return diff


def read_backends(filename):
    """Read a 'backends' file."""
    backends = {}
    with open(filename, 'r') as fd:
        for line in fd.readlines():
            if not line.strip() or line.startswith('#'):
                continue
            match = MYSQL_URI_RE.search(line.strip())
            if match is None:
                log.error('Could not parse %s line: %s', filename, line)
                return None
            b = {}
            bid, b['user'], b['pass'], b['host'], b['port'], b['db'] = match.groups()
            backends['backend_%d' % int(bid)] = b
    return backends


def read_json_file(filename):
    """Read a plain JSON-encoded file."""
    with open(filename, 'r') as fd:
        return json.load(fd)


def main():
    parser = optparse.OptionParser()
    parser.add_option('--old-map', dest='old_map',
                      help='Old blog/backend map (JSON)')
    parser.add_option('--old-backends', dest='old_backends',
                      help='Old backends list')
    parser.add_option('--new-map', dest='new_map',
                      help='New blog/backend map (JSON)')
    parser.add_option('--new-backends', dest='new_backends',
                      help='New backends list')
    parser.add_option('--state-file', dest='state_file',
                      default='noblogsmv.state',
                      help='Location of the state file (default: %default)')
    parser.add_option('--port', dest='port', type='int',
                      default=3030,
                      help='Port for the web interface (default: %default)')
    parser.add_option('--debug', action='store_true')
    opts, args = parser.parse_args()

    if len(args) != 0:
        parser.error('Wrong number of args')
    for a in ('old_map', 'old_backends', 'new_map', 'new_backends'):
        if not getattr(opts, a):
            parser.error('Must specify --%s' % a.replace('_', '-'))

    logging.basicConfig(
        level=(logging.DEBUG if opts.debug else logging.INFO))

    diff = generate_diff_from_maps(
        read_json_file(opts.old_map),
        read_backends(opts.old_backends),
        read_json_file(opts.new_map),
        read_backends(opts.new_backends))

    for key, value in diff.iteritems():
        state.write_work_unit(sys.stdout, key, value)


if __name__ == '__main__':
    main()
