import glob
import logging
import optparse
import os
import re
import subprocess
import sys
import collections

from noblogsmv import state
from noblogsmv import webapp
from functools import partial

BLOGS_DIR = '/opt/noblogs/www/wp-content/blogs.dir'
MYSQL_URI_RE = re.compile(
    r'^(\d+)\s+\S+\s+mysql://([^:]+):([^@]+)@([^:]+):(\d+)/(\S+)')

log = logging.getLogger(__name__)

dry_run = False


def execute_gen(cmd, dry_run=False):
    if dry_run:
        log.info("[DR] %s" % cmd)
        return ''
    return subprocess.check_output(cmd)


def init(blog_id, value, progress):
    return 'move_data'


def init_clean(blog_id, value, progress):
    return 'rm_data'


def sshcmd(*args):
    return [
        'ssh',
        '-o', 'BatchMode=yes',
        '-o', 'StrictHostKeyChecking=no',
        '-o', 'ControlMaster=auto',
        '-o', 'ControlPath=.ssh_ctrl_%h',
        '-o', 'ControlPersist=yes',
    ] + list(args)


def ssh_cleanup():
    log.info('cleaning up SSH control sockets...')
    for path in glob.glob('.ssh_ctrl_*'):
        subprocess.check_call([
            'ssh',
            '-o', 'ControlMaster=auto',
            '-o', 'ControlPath=%s' % path,
            '-O', 'exit',
            'unused'])


class MysqlNoblogs(object):
    conn = collections.namedtuple('DbData',
                                  ['host', 'port', 'user', 'password', 'db'])

    def __init__(self, old_host, old_port, old_user, old_pass, old_db,
                 new_host, new_port, new_user, new_pass, new_db, **kwd):
        self.old = self.conn(old_host, old_port, old_user, old_pass, old_db)
        self.new = self.conn(new_host, new_port, new_user, new_pass, new_db)

    def options(self, conn):
        return '-h%s -P%s -u%s -p%s' % (conn.host, conn.port,
                                        conn.user, conn.password)

    @property
    def old_options(self):
        return self.options(self.old)

    @property
    def new_options(self):
        return self.options(self.new)

    def find_tables(self, blog_id):
        find_tables =['mysql', self.options(self.old), self.old.db, '-NBe',
                       "'show tables like \"wp\\_%s\\_%%\"'" % blog_id]
        cmd = sshcmd('root@%s' % self.old.host, '%s' % ' '.join(find_tables))
        log.debug('Calling %s', ' '.join(cmd))
        raw_tables = subprocess.check_output(cmd)
        self.tables = [ t.strip() for t in raw_tables.splitlines()]


def process_move_data(blog_id, value, progress):
    progress.update('moving blog data')
    log.info('moving data for %s', blog_id)
    blog_dir = os.path.join(BLOGS_DIR, blog_id)

    if not dry_run:
        # Only run rsync if the directory actually exists.
        cmd = sshcmd(
            'root@%s' % value['old_host'],
            'test ! -d %s || rsync -a %s/ root@%s:%s/' % (
                blog_dir, blog_dir, value['new_host'], blog_dir))
        log.debug('Calling %s', ' '.join(cmd))
        try:
            execute(cmd)
        except (subprocess.CalledProcessError, OSError), e:
            progress.update('moving blog data failed, retrying')
            log.info('Could not move data for blog %s: %s', blog_id, e)
            return None

    else:
        cmd = sshcmd(
            'root@%s' % value['old_host'],
            'du', '-s', blog_dir)
        try:
            usage = int(subprocess.check_output(cmd).split()[0])
            log.info('blog %s on %s usage: %d Kb', blog_id, value['old_host'], usage)
        except (subprocess.CalledProcessError, OSError, ValueError), e:
            log.error('error retrieving usage for blog %s@%s: %s',
                      blog_id, value['old_host'], e)

    progress.update('moved blog data')
    log.info('Moved data for blog %s', blog_id)
    return 'move_db'


def process_move_db(blog_id, value, progress):
    progress.update('moving blog database')
    log.info('moving database for %s', blog_id)
    db = MysqlNoblogs(**value)
    try:
        db.find_tables(blog_id)
    except (subprocess.CalledProcessError, OSError), e:
        progress.update('could not look up blog tables, retrying')
        log.info('Could not look up tables for blog %s: %s', blog_id, e)
        return None

    if not db.tables:
        log.info('Could not find any table for blog %s on host %s',
                 blog_id, value['old_host'])
        #Not sure this is the best option: if we have *no* tables,
        #we should probably not retry, but declare the move failed for good.
        return state.STATE_ERROR

    tables = ' '.join(db.tables)
    move_db = ['mysqldump', '--opt', db.old_options, value['old_db'], tables,
               '|', 'mysql', db.new_options, value['new_db']]
    cmd = sshcmd(
           'root@%s' % value['old_host'], '%s' % ' '.join(move_db))
    log.debug('Calling %s', ' '.join(cmd))
    try:
        execute(cmd)
    except (subprocess.CalledProcessError, OSError), e:
        progress.update('could not move blog database, retrying')
        log.info('Could not move blog %s database: %s', blog_id, e)
        return None

    return state.STATE_DONE


def process_rm_db(blog_id, value, progress):
    progress.update('removing blog in old location')
    log.info('removing data of blog %s from %s', blog_id,
             value['old_server_id'])

    db = MysqlNoblogs(**value)
    try:
        db.find_tables(blog_id)
    except (subprocess.CalledProcessError, OSError), e:
        progress.update('could not look up blog tables, retrying')
        log.info('Could not look up tables for blog %s: %s', blog_id, e)
        return None

    if not db.tables:
        return state.STATE_DONE

    tables = ', '.join(db.tables)
    drop_db = ['mysql', db.old_options, db.old.db, '-NBe',
               '"DROP TABLE %s"' % tables]
    cmd = sshcmd(
        'root@%s' % value['old_host'], '%s' % ' '.join(drop_db))
    try:
        execute(cmd)
    except (subprocess.CalledProcessError, OSError), e:
        progress.update('could not remove old blog database, retrying')
        log.info('Could not remove blog %s database: %s', blog_id, e)
        return None
    progress.update('removed blog db; Done.')
    return state.STATE_DONE


def process_rm_data(blog_id, value, progress):
    progress.update('removing blog data files from old location')
    log.info('Removing blog data files')
    blog_dir = os.path.join(BLOGS_DIR, blog_id)
    cmd = sshcmd(
        'root@%s' % value['old_host'], 'rm', '-rf',
        '%s/' % blog_dir)
    log.debug('Calling %s', ' '.join(cmd))
    try:
        execute(cmd)
    except (subprocess.CalledProcessError, OSError), e:
        progress.update('removing old blog data failed, retrying')
        log.info('Could not remove data for blog %s: %s', blog_id, e)
        return None

    progress.update('moved blog data')
    log.info('Moved data for blog %s', blog_id)
    return 'rm_db'


class NoblogsStateMachine(state.StateMachine):
    pass


class NoblogsMoveStateMachine(NoblogsStateMachine):

    states = {
        'error': state.nop,
        'init': init,
        'move_data': process_move_data,
        'move_db': process_move_db,
        'done': state.nop,
    }


class NoblogsCleanStateMachine(NoblogsStateMachine):

    states = {
        'error': state.nop,
        'init': init_clean,
        'rm_data': process_rm_data,
        'rm_db': process_rm_db,
        'done': state.nop
    }

def main():
    parser = optparse.OptionParser(usage='''%prog [<OPTIONS>]

Noblogs rebalancing driver. Given a set of blog diffs on standard
input, it will perform the necessary migration tasks to ensure the
system reaches the desired state. An HTTP server allows the user to
monitor the migration progress.

The program keeps its state checkpointed to a local database, so
that its execution can be resumed if interrupted. In this case,
rather than passing the data again on standard input, one should
use the `--recover' option.

''')
    parser.add_option('--state', dest='state_file',
                      default='noblogsmv.state',
                      help='Location of the state file (default: %default)')
    parser.add_option('--port', dest='port', type='int',
                      default=3030,
                      help='Port for the web interface (default: %default)')
    parser.add_option('--num-workers', dest='num_workers', type='int',
                      default=20,
                      help='Number of workers (default: %default)')
    parser.add_option('--dry-run', dest='dry_run', action='store_true',
                      default=False, help='Do not really perform the migration.')
    parser.add_option('--recover', dest='recover', action='store_true',
                      help='Recover a previous run')
    parser.add_option('--noexit', action='store_true',
                      help='Do not exit when done')
    parser.add_option('--debug', action='store_true')
    parser.add_option('--clean', action='store_true',
                      help='Clean up instead of moving blogs.')
    opts, args = parser.parse_args()

    if len(args) != 0:
        parser.error('Wrong number of args')

    logging.basicConfig(
        level=(logging.DEBUG if opts.debug else logging.INFO))

    global execute
    execute = partial(execute_gen, dry_run=opts.dry_run)

    global dry_run
    dry_run = opts.dry_run

    if opts.clean:
        statemachine = NoblogsCleanStateMachine
    else:
        statemachine = NoblogsMoveStateMachine

    inputfd = None
    if not opts.recover:
        inputfd = sys.stdin
    sm, httpserver = statemachine.new_with_fd(
        opts.state_file,
        inputfd=inputfd,
        num_workers=opts.num_workers,
        enable_http=True,
        http_port=opts.port)

    sm.run()

    ssh_cleanup()

    if opts.noexit:
        # This will never return.
        httpserver.join()


if __name__ == '__main__':
    main()
