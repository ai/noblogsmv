import contextlib
import json
import logging
import os
import sys
import threading
import time
import Queue

import leveldb


log = logging.getLogger(__name__)

nop = lambda k, v, progress: v

# Fixed (not application-defined) states.
STATE_INIT = 'init'
STATE_DONE = 'done'
STATE_ERROR = 'error'

EXIT_STATES = (STATE_DONE, STATE_ERROR)


# The base work unit has a primary key, a state, and some opaque data.
class WorkUnit(object):

    def __init__(self, key, state, data):
        self.key = key
        self.state = state
        self.data = data

    def __eq__(self, b):
        return ((self.key == b.key) and
                (self.state == b.state) and
                (self.data == b.data))


from json import JSONEncoder, JSONDecoder

class _WUEncoder(JSONEncoder):

    def default(self, o):
        return o.__dict__


def from_json(json_object):
    if 'key' in json_object:
        return WorkUnit(json_object['key'],
                        json_object['state'],
                        json_object['data'])
    return json_object

WUEncoder = _WUEncoder()
WUDecoder = JSONDecoder(object_hook=from_json)


class LevelDbSession(object):

    def __init__(self, db):
        self.db = db
        self.snap = db.CreateSnapshot()

    def _decode(self, obj):
        if obj:
            return WUDecoder.decode(obj)

    def _encode(self, obj):
        return WUEncoder.encode(obj)

    def close(self):
        del self.snap

    def put(self, key, value):
        self.db.Put(key, self._encode(value))

    def get(self, key):
        return self._decode(self.snap.Get(key))

    def put_many(self, values):
        wb = leveldb.WriteBatch()
        for key, value in values:
            wb.Put(key, self._encode(value))
        self.db.Write(wb)

    def scan(self, keys_only=False):
        iter = self.snap.RangeIter(key_from='',
                                   key_to='\xff',
                                   fill_cache=False,
                                   include_value=not keys_only)
        for datum in iter:
            if keys_only:
                yield datum
            else:
                yield datum[0], self._decode(datum[1])


class StateDatabase(object):
    """Simple key-value database that encodes data with a codec."""

    def __init__(self, path, codec=json):
        self.db = leveldb.LevelDB(
            path,
            block_cache_size=256 * (1 << 20))
        self.codec = codec

    def session(self):
        return LevelDbSession(self.db)

    def close(self):
        pass

    def put(self, session, work):
        session.put(work.key, work)

    set = put

    def get(self, session, key):
        return session.get(key)

    def put_many(self, session, values):
        session.put_many(values)

    def scan(self, session, only_pending=True):
        """Iterate over all the database keys."""
        result = []
        for key, value in session.scan():
            if only_pending and value.state in EXIT_STATES:
                continue
            result.append(key)
        return result

    def dump(self, session):
        """Return every object in the db."""
        return [x[1] for x in session.scan()]

    def are_we_done(self, session):
        """Check if all the entries are in a final state."""
        for key, value in session.scan():
            if value.state not in EXIT_STATES:
                return False
        return True

    def count_by_state(self, session):
        count = {}
        for key, value in session.scan():
            if value.state in count:
                count[value.state] += 1
            else:
                count[value.state] = 1
        return count


@contextlib.contextmanager
def transaction(db):
    session = db.session()
    try:
        yield session
    finally:
        session.close()

readonly_transaction = transaction


@contextlib.contextmanager
def work_transaction(db, key):
    with transaction(db) as session:
        work = db.get(session, key)
        yield work
        db.put(session, work)


class WorkerProgressReporter(object):

    def __init__(self, worker):
        self.worker = worker

    def update(self, message):
        self.worker.update_info({'progress': message})


class StateMachineWorker(threading.Thread):
    """A worker.

    The worker algorithm is very simple: it reads one work unit from
    the incoming queue, processes it according to the state definition
    table, and eventually re-injects it into the queue if the result
    state is not final.

    Each worker maintains an 'info' dictionary with some data on the
    current state.
    """

    def __init__(self, sm, input_queue, thread_id):
        threading.Thread.__init__(self, name='Worker %d' % thread_id)
        self.sm = sm
        self.input_queue = input_queue
        self.thread_id = thread_id
        self._info_lock = threading.Lock()
        self._info = {'state': 'init'}

    def run(self):
        log.debug('worker %d started', self.thread_id)
        while True:
            self.set_info({'state': 'idle'})

            key = self.input_queue.get()
            if key is None:
                break

            log.debug('worker %d received key "%s"', self.thread_id, key)
            try:
                with work_transaction(self.sm.db, key) as work:
                    self.set_info({'state': 'running',
                                   'key': key,
                                   'value': work.data,
                                   'started_at': time.time()})
                    progress = WorkerProgressReporter(self)
                    new_state = self.sm.process(key, work.state, work.data, progress)
                    log.debug('worker %d processed "%s": %s -> %s',
                              self.thread_id, key, work.state, new_state)
                    work.state = new_state

                # Reinject if not done.
                if new_state not in EXIT_STATES:
                    self.input_queue.put(key)
            except Exception, e:
                log.exception('Unexpected exception')
                self.input_queue.put(key)
                continue

        log.debug('worker %d exiting', self.thread_id)

    def set_info(self, info):
        with self._info_lock:
            self._info = info

    def update_info(self, d):
        with self._info_lock:
            self._info.update(d)

    @property
    def info(self):
        with self._info_lock:
            info = dict(self._info)
        info['thread_id'] = self.name
        return info


class RingBuffer(object):

    def __init__(self, size):
        self.size = size
        self.values = [None] * size
        self.ptr = 0

    def add(self, value):
        self.values[self.ptr] = value
        self.ptr += 1
        if self.ptr > self.size:
            self.ptr = 0

    def get_values(self):
        return self.values[self.ptr:] + self.values[:self.ptr]


class StatsThread(threading.Thread):

    def __init__(self, sm):
        threading.Thread.__init__(self)
        self.sm = sm
        self._stop = threading.Event()

    def stop(self):
        self._stop.set()

    def run(self):
        while not self._stop.isSet():
            self._stop.wait(10)
            try:
                self.sm.compute_stats()
            except:
                pass


class StateMachine(object):

    # Subclasses should override this, retaining the 'error' rule.
    states = {
        'error': nop,
        }

    def __init__(self, num_workers, dbfile):
        self.db = StateDatabase(dbfile)
        self.running = False
        self.num_workers = num_workers
        self.threads = []
        self.state_count = {}
        self.state_count_ts = {}

    def load_data(self, input_stream):
        with transaction(self.db) as session:
            self.db.put_many(session, 
                ((key, WorkUnit(key, STATE_INIT, value))
                 for key, value in input_stream))

    def process(self, key, state, value, progress):
        value.pop('error_msg', '')

        method = self.states.get(state)
        if method is None:
            log.error('unknown state "%s" while processing %s: %s',
                      state, key, str(value))
            state = 'error'
        else:
            try:
                new_state = method(key, value, progress)
                if new_state:
                    state = new_state
            except Exception, e:
                log.exception('exception while processing %s: %s',
                              key, str(value))
                progress.update(str(e))
                value['error_msg'] = str(e)
                state = 'error'

        value['count'] = value.get('count', 0) + 1
        return state

    def get_worker_info(self):
        return [t.info for t in self.threads]

    def get_state(self):
        with readonly_transaction(self.db) as session:
            return self.db.dump(session)

    def get_current_state_count(self):
        return self.state_count

    def get_ts_state_count(self):
        return self.state_count_ts

    def compute_stats(self):
        with readonly_transaction(self.db) as session:
            state_count = self.db.count_by_state(session)
            state_count_ts = dict(self.state_count_ts)
            for key, value in state_count.iteritems():
                if key not in state_count_ts:
                    state_count_ts[key] = RingBuffer(128)
                state_count_ts[key].add(value)
            self.state_count = state_count
            self.state_count_ts = state_count_ts

    def run(self):
        input_queue = Queue.Queue()
        self.threads = [
            StateMachineWorker(self, input_queue, i + 1)
            for i in xrange(self.num_workers)]

        stats_thread = StatsThread(self)
        stats_thread.start()

        # Inject initial state.
        with readonly_transaction(self.db) as session:
            for key in self.db.scan(session):
                input_queue.put(key)

        # Start the workers after all the data has been loaded.
        [x.start() for x in self.threads]
        self.running = True

        # Wait until everything is done.
        while True:
            try:
                with readonly_transaction(self.db) as session:
                    if self.db.are_we_done(session):
                        break
            except levelDb.LevelDBError:
                pass
            time.sleep(3)

        # Kill the workers.
        for i in xrange(self.num_workers):
            input_queue.put(None)
        [x.join() for x in self.threads]
        stats_thread.stop()
        stats_thread.join()
        self.running = False

        self.compute_stats()

    @classmethod
    def new_with_fd(cls, dbfile, inputfd=None, num_workers=10,
                    enable_http=False, http_port=3030):
        sm = cls(num_workers, dbfile)

        if enable_http:
            from noblogsmv import webapp
            httpserver = webapp.WsgiThread(sm, http_port)
            httpserver.start()

        if inputfd:
            def _read_values():
                for line in inputfd:
                    key, value = line.strip().split(' ', 1)
                    yield key, json.loads(value)
            sm.load_data(_read_values())

        if enable_http:
            return sm, httpserver

        return sm


def write_work_unit(fd, key, data):
    fd.write('%s %s\n' % (key, json.dumps(data)))
