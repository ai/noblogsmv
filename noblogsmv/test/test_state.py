import collections
import os
import shutil
import tempfile
import threading
import time
import unittest
import urllib2
from noblogsmv import state

import leveldb


def _test_work_unit():
    return state.WorkUnit('key', state.STATE_INIT, {'a': 42})


class TestBase(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)


class DatabaseTest(TestBase):

    def setUp(self):
        TestBase.setUp(self)
        self.db = state.StateDatabase(
            os.path.join(self.tmpdir, 'test.db'))

    def tearDown(self):
        self.db.close()
        TestBase.tearDown(self)

    def test_put(self):
        t = _test_work_unit()
        key = t.key
        with state.transaction(self.db) as session:
            self.db.put(session, t)
        with state.transaction(self.db) as session:
            w2 = self.db.get(session, key)
            self.assertEquals(_test_work_unit(), w2)

    # def test_put_is_unique(self):
    #     def _saveit():
    #         with state.transaction(self.db) as session:
    #             self.db.put(session, _test_work_unit())
    #     _saveit()
    #     self.assertRaises(leveldb.LevelDBError, _saveit)

    def test_set(self):
        t = _test_work_unit()
        key = t.key
        with state.transaction(self.db) as session:
            self.db.put(session, t)
        with state.transaction(self.db) as session:
            w = self.db.get(session, key)
            w.data['b'] = 21
            self.db.set(session, w)
        with state.transaction(self.db) as session:
            w2 = self.db.get(session, key)
            self.assertEquals({'a': 42, 'b': 21}, w2.data)

    def test_scan(self):
        with state.transaction(self.db) as session:
            self.db.put(session, state.WorkUnit('key1', state.STATE_INIT, {}))
            self.db.put(session, state.WorkUnit('key2', state.STATE_DONE, {}))
            self.db.put(session, state.WorkUnit('key3', 'mystate', {}))
        with state.transaction(self.db) as session:
            result = set(self.db.scan(session))
            self.assertEquals(set(['key1', 'key3']), result)

    def test_dump(self):
        with state.transaction(self.db) as session:
            self.db.put(session, state.WorkUnit('key1', state.STATE_INIT, {}))
            self.db.put(session, state.WorkUnit('key2', state.STATE_DONE, {}))
            self.db.put(session, state.WorkUnit('key3', 'mystate', {}))
        with state.transaction(self.db) as session:
            result = list(self.db.dump(session))
            self.assertEquals(3, len(result))

    def test_are_we_done(self):
        t = _test_work_unit()
        key = t.key
        with state.transaction(self.db) as session:
            self.db.put(session, t)
        with state.transaction(self.db) as session:
            self.assertFalse(self.db.are_we_done(session))
            w = self.db.get(session, key)
            w.state = state.STATE_DONE
            self.db.set(session, w)
        with state.transaction(self.db) as session:
            self.assertTrue(self.db.are_we_done(session))

    def test_loadtest(self):
        nkeys = 20
        nloops = 100
        out = {'errors': 0, 'fatals': 0}

        with state.transaction(self.db) as session:
            for i in xrange(nkeys):
                self.db.put(session, 
                            state.WorkUnit('key%d' % i, state.STATE_INIT, {i: i}))

        def loadfn(i):
            key = 'key%d' % i
            for j in xrange(nloops):
                try:
                    with state.work_transaction(self.db, key) as work:
                        work.state = state.STATE_DONE
                except leveldb.LevelDBError:
                    out['errors'] += 1
                except Exception, e:
                    print e
                    out['fatals'] += 1
                    return

        threads = [threading.Thread(target=loadfn, args=(i,)) for i in xrange(nkeys)]
        [x.start() for x in threads]
        [x.join() for x in threads]
        self.assertEquals(0, out['fatals'])
        self.assertLess(out['errors'], 10)


def _mk_state_machine(_states):
    class _MySM(state.StateMachine):
        states = _states
    return _MySM


TEST_DATA = [('key1', {'a': 1}),
             ('key2', {'a': 2}),
             ]

class StateMachineLowLevelTest(TestBase):

    def test_load_data(self):
        smc = _mk_state_machine({'test': state.nop})
        sm = smc(2, os.path.join(self.tmpdir, 'sm.db'))
        sm.load_data(TEST_DATA)
        with state.transaction(sm.db) as session:
            self.assertFalse(sm.db.are_we_done(session))


class StateMachineTest(TestBase):

    def _scrape_data(self, db):
        # Scrape the contents of the db into the test object.
        def _d(w):
            return {'key': w.key, 'state': w.state, 'data': w.data}
        with state.transaction(db) as session:
            self.output_data = [_d(db.get(session, x))
                                for x in db.scan(session, only_pending=False)]

    def _run_sm(self, states, inputs, timeout=10):
        smc = _mk_state_machine(states)
        sm = smc(2, os.path.join(self.tmpdir, 'sm.db'))
        sm.load_data(inputs)
        t = threading.Thread(target=sm.run)
        t.setDaemon(True)
        t.start()
        print 'waiting %d seconds for test to complete...' % timeout
        t.join(timeout=timeout)
        self.assertFalse(sm.running)
        self._scrape_data(sm.db)

    def test_sm_1(self):
        # Test a simple, one-level state machine.
        counters = collections.defaultdict(int)
        def incr_fn(key, value, progress_cb):
            counters[key] += 1
            return state.STATE_DONE
        self._run_sm({'init': incr_fn}, TEST_DATA)
        self.assertEquals({'key1': 1, 'key2': 1}, counters)

    def test_sm_2(self):
        # Test a two-level state machine. The increments are performed
        # on the second step.
        counters = collections.defaultdict(int)
        def incr_fn(key, value, progress_cb):
            counters[key] += 1
            return state.STATE_DONE
        def bridge_fn(key, value, progress_cb):
            return 'incr'
        self._run_sm(
            {'init': bridge_fn,
             'incr': incr_fn},
            TEST_DATA)
        self.assertEquals({'key1': 1, 'key2': 1}, counters)

    def test_sm_3(self):
        # Test data changes. It should be possible to modify the value
        # object in-place.
        def mod_fn(key, value, progress_cb):
            value['b'] = 42
            return state.STATE_DONE
        self._run_sm({'init': mod_fn}, TEST_DATA)
        bvalues = [w['data']['b'] for w in self.output_data]
        self.assertEquals([42, 42], bvalues)


class WebappTest(TestBase):

    def setUp(self):
        TestBase.setUp(self)

    def tearDown(self):
        TestBase.tearDown(self)
        if hasattr(self, 'httpserver'):
            self.httpserver.stop()

    def test_webapp(self):
        port = 31313
        smc = _mk_state_machine({'init': state.nop})
        sm, self.httpserver = state.StateMachine.new_with_fd(
            os.path.join(self.tmpdir, 'web.db'),
            enable_http=True,
            http_port=port)

        time.sleep(0.1)

        # Check that the index page loads even if there isn't any data.
        resp = urllib2.urlopen('http://127.0.0.1:%d/' % port)
        body = resp.read()
        self.assertEquals(200, resp.code)
        self.assertTrue('<title>' in body)

        sm.load_data(TEST_DATA)

        # Check the index page again, now with data.
        resp = urllib2.urlopen('http://127.0.0.1:%d/' % port)
        body = resp.read()
        self.assertEquals(200, resp.code)
        self.assertTrue('key1' in body)
