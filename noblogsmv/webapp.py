import collections
import logging
import os
import socket
import threading
import time
import urllib2
from noblogsmv import state
from flask import Flask, redirect, render_template, request, g
from pygooglechart import Axis, SimpleLineChart


app = Flask(__name__)


@app.before_request
def before_request_handler():
    g.sm = app.config.sm


_palette = ['ffa07a',
            'ffdab9',
            '5f9ea0',
            'dda0dd',
            '6294ed',
            '8fbc8b',
            'f0e68c',
            'ff69b4',
            '4ce2d3',
            ]


def _mk_stacked_graph(chart, valuelist):
    if valuelist:
        n = len(valuelist[0])
    else:
        n = 1
    v0 = [0] * n
    chart.add_data(v0)
    for j, values in enumerate(valuelist):
        for i, v in enumerate(values):
            if v:
                v0[i] += v
        chart.add_data(v0[:])
        chart.add_fill_range(_palette[j], j, j+1)
    chart.set_colours(['000000'] + _palette[:n])


def _mk_stats_graph(x=500, y=270):
    ts = g.sm.state_count_ts

    labels = sorted(ts.keys())
    valuelist = []
    for key in labels:
        valuelist.append(ts[key].get_values())

    chart = SimpleLineChart(x, y)
    _mk_stacked_graph(chart, valuelist)
    chart.set_legend([''] + labels)
    return chart.get_url()


@app.route('/')
def index():
    now = time.time()
    cur_state = g.sm.get_state()
    worker_info = g.sm.get_worker_info()
    state_count = g.sm.state_count
    return render_template('index.html',
                           state=cur_state,
                           state_count=state_count,
                           state_count_graph=_mk_stats_graph(),
                           worker_info=worker_info,
                           now=time.time())


@app.route('/quitquitquit')
def shutdown():
    # This will stop the default Werkzeug development server.
    halt_fn = request.environ.get('werkzeug.server.shutdown')
    if halt_fn:
        halt_fn()
    return 'ok'


def create_app(sm, port, config={}):
    app.config.update(
        SECRET_KEY=os.urandom(8))
    app.config.from_pyfile('APP_CONFIG', silent=True)
    app.config.update(
        HOST_NAME=socket.gethostname(),
        HOST_PORT=port)
    app.config.update(config)
    app.config.sm = sm
    return app


class WsgiThread(threading.Thread):

    def __init__(self, sm, port):
        threading.Thread.__init__(self)
        self.sm = sm
        self.port = port
        self.setDaemon(True)

    def run(self):
        wsgiapp = create_app(self.sm, self.port)
        wsgiapp.run(host='0.0.0.0', port=self.port)

    def stop(self):
        urllib2.urlopen('http://127.0.0.1:%d/quitquitquit' % self.port).read()

